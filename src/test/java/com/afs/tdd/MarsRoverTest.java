package com.afs.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class MarsRoverTest {
    @Test
    void should_change_to_0_1_North_when_executeCommand_given_location_0_0_N_and_command_move() {
        //Given
        Location locationInitial = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Move;
        //When
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //Then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(1, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.North, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_1_0_E_when_executeCommand_given_location_0_0_E_and_command_move() {
        //Given
        Location locationInitial = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Move;
        //When
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //Then
        Assertions.assertEquals(1, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.East, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_burden_1_S_when_executeCommand_given_location_0_0_S_and_command_move() {
        //Given
        Location locationInitial = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Move;
        //When
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //Then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(-1, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.South, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_burden_1_0_W_when_executeCommand_given_location_0_0_W_and_command_move() {
        //Given
        Location locationInitial = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Move;
        //When
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //Then
        Assertions.assertEquals(-1, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.West, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_burden_1_0_W_when_executeCommand_given_location_0_0_N_and_command_left() {
        //Given
        Location locationInitial = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Left;
        //When
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //Then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.West, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_1_N_when_executeCommand_given_location_0_0_E_and_command_left() {
        //Given
        Location locationInitial = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Left;
        //When
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //Then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.North, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_1_0_E_when_executeCommand_given_location_0_0_S_and_command_left() {
        //Given
        Location locationInitial = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Left;
        //When
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //Then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.East, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_burden_1_S_when_executeCommand_given_location_0_0_W_and_command_left() {
        //Given
        Location locationInitial = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Left;
        //When
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //Then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.South, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_1_0_E_when_executeCommand_given_location_0_0_N_and_command_right() {
        //Given
        Location locationInitial = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Right;
        //When
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //Then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.East, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_burden_1_S_when_executeCommand_given_location_0_0_E_and_command_right() {
        //Given
        Location locationInitial = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Right;
        //When
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //Then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.South, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_burden_1_0_W_when_executeCommand_given_location_0_0_S_and_command_right() {
        //Given
        Location locationInitial = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Right;
        //When
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //Then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.West, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_1_N_when_executeCommand_given_location_0_0_W_and_command_right() {
        //Given
        Location locationInitial = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Right;
        //When
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //Then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.North, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_burden_1_1_N_when_executeBatchCommands_given_location_0_0_N_and_command_move_left_move_right() {
        //Given
        Location locationInitial = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(locationInitial);
        List<Command> commandMoveList = new ArrayList<>();
        commandMoveList.add(Command.Move);
        commandMoveList.add(Command.Left);
        commandMoveList.add(Command.Move);
        commandMoveList.add(Command.Right);
        //When
        Location locationAfterMove = marsRover.executeBatchCommands(commandMoveList);
        //Then
        Assertions.assertEquals(-1, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(1, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.North, locationAfterMove.getDirection());
    }
}
