package com.afs.tdd;

import java.util.List;

public class MarsRover {
    private Location location;

    public MarsRover(Location location) {
        this.location = location;
    }

    public Location executeCommand(Command commandMove) {
        if (Command.Move.equals(commandMove)) {
            move();
        } else if (Command.Left.equals(commandMove)) {
            turnLeft();
        } else if (Command.Right.equals(commandMove)) {
            turnRight();
        }
        return location;
    }

    private void turnRight() {
        if (Direction.North.equals(location.getDirection())) {
            location.setDirection(Direction.East);
        } else if (Direction.East.equals(location.getDirection())) {
            location.setDirection(Direction.South);
        } else if (Direction.South.equals(location.getDirection())) {
            location.setDirection(Direction.West);
        } else if (Direction.West.equals(location.getDirection())) {
            location.setDirection(Direction.North);
        }
    }

    private void turnLeft() {
        if (Direction.North.equals(location.getDirection())) {
            location.setDirection(Direction.West);
        } else if (Direction.East.equals(location.getDirection())) {
            location.setDirection(Direction.North);
        } else if (Direction.South.equals(location.getDirection())) {
            location.setDirection(Direction.East);
        } else if (Direction.West.equals(location.getDirection())) {
            location.setDirection(Direction.South);
        }
    }

    private void move() {
        if (Direction.North.equals(location.getDirection())) {
            location.setCoordinateX(location.getCoordinateX());
            location.setCoordinateY(location.getCoordinateY() + 1);
            location.setDirection(location.getDirection());
        } else if (Direction.East.equals(location.getDirection())) {
            location.setCoordinateX(location.getCoordinateX() + 1);
            location.setCoordinateY(location.getCoordinateY());
            location.setDirection(location.getDirection());
        } else if (Direction.South.equals(location.getDirection())) {
            location.setCoordinateX(location.getCoordinateX());
            location.setCoordinateY(location.getCoordinateY() - 1);
            location.setDirection(location.getDirection());
        } else if (Direction.West.equals(location.getDirection())) {
            location.setCoordinateX(location.getCoordinateX() - 1);
            location.setCoordinateY(location.getCoordinateY());
            location.setDirection(location.getDirection());
        }
    }

    public Location executeBatchCommands(List<Command> commandMoveList) {
        for (Command command : commandMoveList) {
            location = executeCommand(command);
        }
        return location;
    }
}
